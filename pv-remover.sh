#!/bin/bash
#
# PV Remover версия 1.0 (15.03.2021)
# Автор: Барсуков В.В.
# Лицензия GPLv2 или выше.
#
# Описание:
# Средство удаления "застрявших" PV (находящихся в стадии Terminating)
#
# Использование:
# pv-remover.sh
#

# ------------------------------------------------------------------
# Версия и дата
VERNUM="1.0"
VERDATE="15.03.2021"
# ------------------------------------------------------------------
# Меняем локаль на ru_RU.UTF-8
export LC_ALL=ru_RU.UTF-8

# Выводим шапку
echo
echo "+------------------------------------------------+"
echo "| Средство удаления PV. Версия $VERNUM от $VERDATE |"
echo "| PV Remover (PR)                                |"
echo "| (C) Барсуков В.В.                              |"
echo "+------------------------------------------------+"
echo "| Формат: pv-remover.sh PV                       |"
echo "+------------------------------------------------+"

# Если число передаваемых параметров меньше 1
if [[ "$#" -lt "1" ]]; then
    echo "PR: Ошибка. Нет обязательного аргумента."
    echo "PR: Доступные для удаления PV:"
    kubectl get pv | grep "Terminating"	
    exit 1
fi

# Получаем аргумент
PV=$1

# Проверка наличия PV. Если в выходе "NotFound" - ругаемся и выходм
STATUS=$(kubectl get pv $PV | grep "NotFound")
if ! [[ -z "$STATUS" ]]; then
    echo "PR: Ошибка. PV $PV не найден."
    echo "PR: Доступные для удаления PV:"
    kubectl get pv | grep "Terminating"
    exit 2
fi

# Проверка статуса PV (удаляем только PV в статусе Terminating)
STATUS=$(kubectl get pv $PV | grep "Terminating")
if [[ -z "$STATUS" ]]; then
    echo "PR: Ошибка. PV $PV не подходит для удаления."
    echo "PR: Доступные для удаления PV:"
    kubectl get pv | grep "Terminating"
    exit 3
fi

# Удаляем PV
echo "PR: Удаляем PV $PV"
kubectl patch pv $PV -p '{"metadata": {"finalizers": null}}'
kubectl delete pv $PV --grace-period=0 --force

# Проверяем удален ли PV
echo "PR: Проверяем удален ли PV $PV:"
kubectl get pv $PV

# Выход
exit 0
